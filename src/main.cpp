#include <iostream>
#include <iomanip>
#include <stack>

class node
{
public:
	node() {};
	node(int val) : value(val) 
	{
		left = nullptr;
		right = nullptr;
	};

	int value;
	node* left;
	node* right;
};

enum Direction {
	LEFT, RIGHT
};

class memory_vector
{
public:
	memory_vector(Direction d, node* node_) : dir(d), base(node_) {}

	Direction dir;
	node* base;
};

class pack
{
public:
	std::stack<memory_vector> splay_stack;

	void put(memory_vector d)
	{
		splay_stack.push(d);
	}

	memory_vector& top()
	{
		return splay_stack.top();
	}

	bool is_empty()
	{
		return splay_stack.empty();
	}

	void print()
	{
		std::cout << "Stack ";
		std::stack<memory_vector> tmp = this->splay_stack;
		while(!tmp.empty())
		{
			std::cout << (int) tmp.top().dir 
					  << "(" << tmp.top().base->value << ") ";
			tmp.pop();
		}

		std::cout << std::endl;
	}

};

class splay_tree
{
public:
	node* base_node;

	splay_tree() : base_node(nullptr) {}

	node* begin()
	{
		return base_node;
	}

	/* Deprecated */
	void insert(int value)
	{
		if(base_node == nullptr)
		{
			base_node = new node(value);
			return;
		}

		node* tmp = base_node;
		while(true)
		{
			if(value > tmp->value)
			
				if(tmp->right == nullptr)
				{
					tmp->right = new node(value);
					break;
				}
				else

					tmp = tmp->right;

			else if(value < tmp->value)

				if(tmp->left == nullptr)
				{
					tmp->left = new node(value);
					break;
				}
				else

					tmp = tmp->left;

			else

				break;	
		}

	}

	void print(node* p, int indent = 20)
	{
		if(p != nullptr) {

			if (indent > 0) {
	            std::cout << std::setw(indent) << ' ';
	        }
        	
        	std::cout << p->value << "\n ";

        	if(p->left) print(p->left, indent - 4);
        	if(p->right) print(p->right, indent + 4);
    	}
	}

	void find(int value, pack* package)
	{
		if(base_node == nullptr)

			return;

		node* tmp = base_node;
		while(true)
		{
			if(tmp == nullptr) 
				return;
			
			if(value > tmp->value)
			{	
				package->put(
					memory_vector(Direction::RIGHT, tmp)
				);

				tmp = tmp->right;
			}
			else if(value < tmp->value)
			{
				package->put(
					memory_vector(Direction::LEFT, tmp)
				);

				tmp = tmp->left;
			}

			else

				break;

		}
		
		generate_splay_tree(tmp, package);
	}

private:
	void generate_splay_tree(node* child, pack* package)
	{
			while(child != base_node)
			{
				/* Берем родителя */
				memory_vector parent = package->top();
				
				if(parent.base == base_node)
				
					splay(child, package);
					
				else if(parent.dir == Direction::LEFT)
				{
					package->splay_stack.pop();
					splay(parent.base, package);

					package->put(parent);
					splay(child, package);
				}
				else
				{
					splay(child, package);
					splay(child, package);
				}
			}
	}

	void splay(node* child, pack* package)
	{
			/* Берем родителя */
			memory_vector parent = package->top();
			
			/* Берем родителя родителя */
			package->splay_stack.pop();
			if(!package->is_empty())
			{
				memory_vector parent_parent = package->top();

				get_child_by_direction(parent_parent.base, parent_parent.dir) = child;
			}
			else
				
				base_node = child;

			node*& in_parent = get_child_by_direction(child, (Direction) !parent.dir);
			
			get_child_by_direction(parent.base, parent.dir) = in_parent;
			in_parent = parent.base;
	}

	node*& get_child_by_direction(node* parent, Direction dir)
	{
		switch(dir)
		{
		case Direction::RIGHT:
			return parent->right;
		case Direction::LEFT:
			return parent->left;
		}

		return base_node; /* */
	}
};

int main()
{
	splay_tree splay;

/*	splay.insert(10);
	splay.insert(12);
	splay.insert(3);
	splay.insert(1);

	splay.insert(0);
	splay.insert(2);
	splay.insert(4);*/

/*	splay.insert(10);
	splay.insert(12);
	splay.insert(3);
	splay.insert(1);
	splay.insert(7);
	splay.insert(4);
	splay.insert(8);*/

/*	splay.insert(10);
	splay.insert(5);
	splay.insert(1);
	splay.insert(7);
	splay.insert(12);*/

	splay.print(splay.begin());
	
	pack* p = new pack();

	splay.find(5, p);
}